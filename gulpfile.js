var gulp = require('gulp'),
livereload = require('gulp-livereload'),
gulpIf = require('gulp-if'),
eslint = require('gulp-eslint'),
sass = require('gulp-sass'),
sourcemaps = require('gulp-sourcemaps'),
imagemin = require('gulp-imagemin'),
pngquant = require('imagemin-pngquant'),
csslint = require('gulp-csslint'),
sasslint = require('gulp-sass-lint'),
postcss      = require('gulp-postcss');
autoprefixer = require('autoprefixer');

// Configuration options to generalize Gulpfile.

var sourceLoc = './src';

// Build destination of CSS files.
var styleDest = './static/css/';

// Location of SASS files.
var sassLoc = sourceLoc + '/sass/**/*.scss';

// Location of assets (images, fonts, additional CSS, etc.)
var assetLoc = './static';

gulp.task('default', ['watch']);

gulp.task('imagemin', function () {
  return gulp.src(srcLoc +'/assets/images/*')
  .pipe(imagemin({
    progressive: true,
    svgoPlugins: [{removeViewBox: false}],
    use: [pngquant()]
  }))
  .pipe(gulp.dest(assetLoc +'/images'));
});

gulp.task('lint', () => {
  // ESLint ignores files with "node_modules" paths.
  // So, it's best to have gulp ignore the directory as well.
  // Also, Be sure to return the stream from the task;
  // Otherwise, the task may end before the stream has finished.
  return gulp.src(['**/*.js','!node_modules/**'])
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

gulp.task('sass-lint', function() {
  return gulp.src([sassLoc, '!src/sass/bourbon/**/*.scss','!src/sass/neat/**/*.scss'])
  .pipe(sasslint()).pipe(sasslint.format());
});

gulp.task('css-lint', function() {
  gulp.src([assetLoc + '/css/*.css'])
  .pipe(csslint());
});


gulp.task('sass', function () {
  gulp.src(sassLoc)
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
  .pipe(postcss([ autoprefixer() ]))
  .pipe(sourcemaps.write('../maps', {overwrite: true}))
  .pipe(gulp.dest(styleDest, {overwrite: true}));
});

// Needs thinking about: Where does what go?
gulp.task('autoprefixer', function () {
    return gulp.src('.static/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('.static/css'));
});

gulp.task('watch', function(){
  livereload.listen();
  gulp.watch( sassLoc, ['sass','sass-lint']);
  gulp.watch('./src/js/**/*.js', ['eslint']);
  gulp.watch([ sassLoc, 'style.css', assetLoc + '/css/*.css', assetLoc + '/js/*.js'], function (files){
    livereload.changed(files)
  });
});
